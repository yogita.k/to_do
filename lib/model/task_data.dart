import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:todoapp/database/my_database.dart';
import 'package:todoapp/model/detailtask.dart';

class TaskData extends ChangeNotifier {
  final MyDataBase db = MyDataBase.object;
  List<Task> _tasks = [];

  Future<void> getData() async {
    _tasks = await db.getData();
    notifyListeners();
  }

  List<Task> get tasks => _tasks;

  int get taskCount => _tasks.length;

  Future<void> addTask({required String title}) async {
    await db.insertData(task: Task(name: title));
    await getData();
  }

  Future<void> updateTask(Task task) async {
    await db.updateData(id: task.id!, isDone: !task.isDone);
    await getData();
  }

  Future<void> deleteTask({required int id}) async {
    await db.deleteData(id: id);
    await getData();
  }
}

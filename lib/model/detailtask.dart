class Task {
  int? id;
  String? name;
  bool isDone;

  Task({this.id, this.name, this.isDone = false});

  Task.fromMap(Map<String, dynamic> item)
      : id = item["id"],
        name = item["name"],
        isDone = item["isDone"] == 1;

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "isDone": isDone ? 1 : 0,
      };
}

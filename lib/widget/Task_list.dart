import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/model/task_data.dart';
import 'package:todoapp/widget/Task_tile.dart';

class TaskList extends StatefulWidget {
  const TaskList({super.key});

  @override
  State<TaskList> createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 35),
      child: Consumer<TaskData>(
        builder: (context, taskData, child) {
          return ListView.builder(
            itemCount: taskData.taskCount,
            itemBuilder: (context, index) {
              final task = taskData.tasks[index];
              return Dismissible(
                  key: UniqueKey(),
                  direction: DismissDirection.endToStart,
                  onDismissed: (_) {
                    taskData.deleteTask(id: task.id!);
                  },
                  background: Container(
                      alignment: Alignment.centerRight,
                      color: Colors.grey,
                      child: const Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Icon(
                          Icons.delete,
                          size: 30,
                        ),
                      )),
                  child: TaskTile(
                    isChecked: task.isDone,
                    taskTitle: task.name,
                    checkBoxState: (bool? checkBoxState) async {
                      await taskData.updateTask(task);
                    },
                  ));
            },
          );
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Provider.of<TaskData>(context, listen: false).getData();
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/screen/add_task_screen.dart';
import 'package:todoapp/widget/Task_list.dart';
import '../model/task_data.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xFF00008b),
        body: SafeArea(
          child: Column(
            children: [
              Expanded(
                  child: Column(children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Icon(Icons.menu, color: Colors.white),
                      Text(
                        "Tasks",
                        style: TextStyle(color: Colors.white, fontSize: 30),
                      ),
                      Icon(
                        Icons.watch_later_outlined,
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          "   ${Provider.of<TaskData>(context).taskCount} Tasks",
                          style: const TextStyle(
                              fontSize: 25, color: Colors.white),
                        ),
                      ),
                      FloatingActionButton.extended(
                        onPressed: () {
                          showModalBottomSheet(
                            isScrollControlled: true,
                            context: context,
                            builder: (context) {
                              return AddTask();
                            },
                          );
                        },
                        backgroundColor: Colors.white,
                        label: const Text("+ Add Task",
                            style: TextStyle(color: Color(0xFF00008b))),
                      )
                    ],
                  ),
                )
              ])),
              Expanded(
                flex: 3,
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(75))),
                  child: const TaskList(),
                ),
              )
            ],
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoapp/model/task_data.dart';

class AddTask extends StatelessWidget {
  final TextEditingController nameController = TextEditingController();

  AddTask({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFF757575),
      child: Container(
        height: 500,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40))),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              const Center(
                  child: Text(
                "Add Task",
                style: TextStyle(
                    color: Color(0xFF00008b),
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              )),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: nameController,
                  decoration: const InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(50)),
                        borderSide:
                            BorderSide(color: Color(0xFF00008b), width: 4),
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          borderSide: BorderSide(
                            color: Color(0xFF00008b),
                            width: 5,
                          )),
                      hintText: "Enter task here",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          borderSide:
                              BorderSide(width: 20, color: Color(0xFF00008b)))),
                ),
              ),
              ElevatedButton(
                  style: const ButtonStyle(
                      backgroundColor:
                          MaterialStatePropertyAll(Color(0xFF00008b)),
                      fixedSize: MaterialStatePropertyAll(Size(130, 30))),
                  onPressed: () {
                    if (nameController.text.isNotEmpty) {
                      Provider.of<TaskData>(context, listen: false)
                          .addTask(title: nameController.text);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text("enter the task")));
                    }
                    Navigator.pop(context);
                  },
                  child: const Text(
                    "Submit",
                    style: TextStyle(fontSize: 25),
                  )),
              const SizedBox(
                height: 890,
              )
            ],
          ),
        ),
      ),
    );
  }
}
